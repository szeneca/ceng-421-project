# CENG 421 Project

	TODO:
		try filtering on resized versions of images
			different upscaling methods
	
		constrained region growing
			1) I -> upscale -> line detect -> downscale -> J1
			2) I -> line detect -> J2
			3) constrained region grow J2 into J1

		Evaluation:
			false positive rate
			false negative rate
			Precision and Recall
			Connected component precision and recall (extra)
	
		Partition difference
		
		Preprocessing
			adaptive histogram equalization (add u_image to black region to reduce artifacting)
			mask from canny to remove black region


		Mask generation
			canny 
			

		Line Mask generation
			interparc
			interpolate from line eqn is better than circ shifting
			sub2indicies










