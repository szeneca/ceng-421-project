function [p, r, f, ROC] = evaluation(img, gt)
	% perform precision and recall evaluation on an image img compared to its ground truth gt
	
	% alias inputs to local variables (img and gt assumed to be binary matrices)
	A = img;	
	B = gt;

	% get number of pixels in common between both images
	%	(i.e. the ground truth pixels found by the segmentation method)
	AnB = nnz(A.*B);

	% compare to the number of pixles in the ground truth and segmentation result
	p = AnB/nnz(A);		% precision
	r = AnB/nnz(B);		% recall

	% get f-score
	f = 2/((1/p) + (1/r));

	% get TP/FP/TN/FN rates
	[M N] = size(img);
	
	tp = nnz(A.*B)/nnz(B);				% 	TP = #correct in output / #correct in gt
	tn = nnz((1-A).*(1-B))/nnz(1-B);	%	FP = #correct in 1-output / # correct in 1-gt
	fp = 1 - tn;						%	accompanying FP rate
	fn = 1 - tp;						%	accompanying FN rate

	ROC = [tp; fp; tn; fn;];

end
