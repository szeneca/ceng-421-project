clc;
clear 'all';

I = eye(15);
w = zeros(15,15,12);

for i = 1:12
	w(:,:,i) = imrotate(I, -(i-1)*15, 'bicubic', 'crop');
end

figure;

t = 0.5;


for i = 1:12
	b = zeros(15,15);
	b(w(:,:,i) > t) = 1;
	w(:,:,i) = b;
	subplot(3,4,i);
	imshow(w(:,:,i));
end
