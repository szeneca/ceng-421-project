function [pp_img, pp_gt] = postprocess(img, gt, mask, msize)
	% remove the edges from the line detector response
	% resize the ground truth to be consistent with resized image

	% create smaller mask to apply to images
	M = imerode(mask, ones(msize));
	
	% apply mask to remove circumference responses
	I = img.*M;
	J = gt.*M;
	
	% get segmentation threshold
	T = graythresh(img);

	% segment line detector response image
	pp_img = imbinarize(img, T);
	pp_gt = J;

end
