clc;
clear 'all';

% create test results
test_path = 'test-images/stare-images/';
result_path = 'test-results/';

test_images = dir(strcat(test_path, '*.ppm'));
test_images = {test_images.name}';

mkdir('test-results');

prepmasks = [];

for k = 1:length(test_images)
	read_path = strcat(test_path, test_images{k});
	I = imread(read_path);
	[I M] = preprocess(I);
	M = imerode(M, ones(15));
	prepmasks(:,:,k) = M;
	J = msld(I, 15, 'subsegment');
	J = J.*prepmasks(:,:,k);
	images{k} = J;
	write_path = strcat(result_path, test_images{k});
	imwrite(J, write_path);
end
images = images';

% get ground truth images
ah_truth_path = 'test-images/labels-ah/';

ah_images = dir(strcat(ah_truth_path, '*.ppm'));
ah_images = {ah_images.name}';

for k = 1:length(ah_images)
	read_path = strcat(ah_truth_path, ah_images{k});
	ah{k} = imread(read_path);
end
ah = ah';
