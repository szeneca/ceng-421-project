function [J, M] = preprocess(img)
% color to gray scale image
    gsi = rgb2gray(img);

% find the largest piece in our case
    mask = imbinarize(gsi,0.13);

% open and close with se
    se = strel('disk',9);
    mask = imopen(mask, se);
    mask = imclose(mask,se); 
%     mask = imerode(mask,se);

% Apply the mask
    I = bsxfun(@times, img, cast(mask,class(img)));

% return image and mask
	
	[A B ~] = size(img);
	T = ones(A,B);

	J = I;
	M = bsxfun(@times, T, cast(mask,class(T)));

% display preprocess image
%    figure; imshowpair(img,J,'montage');
end
