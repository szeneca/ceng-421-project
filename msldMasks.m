function y = msldMasks(w)
	% return the set of 12 w-by-w masks for a line detector of length w

	% temp override
	w = 15;

	% start by creating the largest length line
	p = (w+1)/2;
	L = zeros(w,w,12);

	%create diagonal lines
	L(:,:,1) = eye(w);
	L(:,:,2) = flipud(L(:,:,1));

	%create vertical lines
	L(:, p, 3) = 1;
	L(:,:,4) = L(:,:,3)';

	% create lines at 15` off of vertical
	% hardcoded for case where w = 15
	L(:,:,5) = L(:,:,3);
	s = floor((w+1)/6);
	L(1:3,:,5) = circshift(L(1:3,:,5), [0 2]);
	L(4:6,:,5) = circshift(L(4:6,:,5), [0 1]);
	% no shift for centre block of 3 pixels
	L(10:12,:,5) = circshift(L(10:12,:,5), [0 -1]);
	L(13:15,:,5) = circshift(L(13:15,:,5), [0 -2]);

	L(:,:,6) = flipud(L(:,:,5));
	L(:,:,7) = L(:,:,5)';
	L(:,:,8) = flipud(L(:,:,7));

	% create lines at 30` angles
	b = zeros(15,15);
	q = imrotate(L(:,:,1), -15, 'bicubic', 'crop');
	b(q > 0.5) = 1;
	L(:,:,9) = b;

	L(:,:,10) = flipud(L(:,:,9));
	L(:,:,11) = L(:,:,9)';
	L(:,:,12) = flipud(L(:,:,11));

	% create smaller lines by sampling a square patch in the center of the largest mask
	masks = cell(ceil(w/2),1);

	c = ceil(w/2);

	masks{1} = L(c,c,:);
	for k = 1:7
		masks{k+1} = L(c-k:c+k, c-k:c+k, :);
	end

	y = masks;

end
